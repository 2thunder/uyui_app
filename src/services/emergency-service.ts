import {Injectable} from "@angular/core";
import {EMERGENCY} from "./mock-emergency";

@Injectable()
export class EmergencyService {
  private emergencies:any;

  constructor() {
    this.emergencies = EMERGENCY;
  }


  getAll() {
    return this.emergencies;
  }

  getItem(id) {
    for (var i = 0; i < this.emergencies.length; i++) {
      if (this.emergencies[i].id === parseInt(id)) {
        return this.emergencies[i];
      }
    }
    return null;
  }

  remove(item) {
    this.emergencies.splice(this.emergencies.indexOf(item), 1);
  }
}