export let RESTAURANTS = [
  {
    id: 1,
    name: "ROASTY CAFÉ & TOSTADURÍA",
      description:"Café de especialidad tostado en Roasty. Para servir, llevar y comprar café molido para tu cafetera. Menú con opción veggie",
    address: "Los Robles #85, Valdivia",
    phone: "976692293",
    location: {
      lat: -39.811092,
      lon: -73.252112
    },
    thumb: "assets/img/restaurant/thumb/img_1.jpg",
      open: "-:- ",
      close: "-:- ",
      categories:[
          {
            name:"cafeteria"
          },
          {
              name:"tostaduría"
          },
      ]
  },
    {
        id: 2,
        name: "CAFÉ SIMPLE",
        description:"Café de especialidad tostado en Roasty. Para servir, llevar y comprar café molido para tu cafetera. Menú con opción veggie",
        address: "Camilo Henriquez #266, Valdivia",
        phone: "973310672",
        location: {
            lat: -39.811312,
            lon: -73.244688
        },
        thumb: "assets/img/restaurant/thumb/img_1.jpg",
        open: "07:30 AM",
        close: "20:00 PM",
        categories:[
            {
                name:"cafeteria"
            },
        ]
    },
    {
        id: 3,
        name: "CAFÉ BARLOVENTO",
        description:"Somos un café valdiviano, pensado en aquellos que necesitan un lugar donde trabajar o simplemente compartir un momento agradable",
        address: "Anibal Pinto #1843, Valdivia",
        phone: "632292101",
        location: {
            lat: -39.825544,
            lon: -73.23988
        },
        thumb: "assets/img/restaurant/thumb/img_1.jpg",
        open: "09:30 AM",
        close: "21:00 PM",
        categories:[
            {
                name:"cafeteria"
            },
        ]
    },
    {
        id: 4,
        name: "ENTRELAGOS",
        description:"Entrelagos es una marca reconocida en la ciudad de Valdivia por la sincronía entre trabajo artesanal y calidad en chocolates y pasteles",
        address: "Pérez Rosales #640, Valdivia",
        phone: "632212047",
        location: {
            lat: -39.81534,
            lon: -73.246456
        },
        thumb: "assets/img/restaurant/thumb/img_1.jpg",
        open: "09:00 AM",
        close: "21:30 PM",
        categories:[
            {
                name:"cafeteria"
            },
        ]
    },
    {
        id: 5,
        name: "CAFÉ MORO",
        description:"La combinación perfecta de buen café, buena comida y buena música",
        address: "Libertad #174, Valdivia",
        phone: "632239084",
        location: {
            lat: -39.813684,
            lon: -73.246592
        },
        thumb: "assets/img/restaurant/thumb/img_1.jpg",
        open: "09:30 AM",
        close: "22:00 PM",
        categories:[
            {
                name:"cafeteria"
            },
        ]
    },
    {
        id: 6,
        name: "CAFÉ DE LUIS",
        description:"En la esquina de la plaza de la república de Valdivia. Sandwich, repostería sureña, cerveza artesanal y el más rico café",
        address: "Maipú #187, Valdivia",
        phone: "632234323",
        location: {
            lat: -39.814696,
            lon: -73.24668
        },
        thumb: "assets/img/restaurant/thumb/img_1.jpg",
        open: "08:30 AM",
        close: "23:00 PM",
        categories:[
            {
                name:"cafeteria"
            },
        ]
    },
];
