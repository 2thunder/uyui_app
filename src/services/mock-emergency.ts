export let EMERGENCY = [
  {
    id: 1,
    name: "GRUA JOAQUIN",
    description: "Servicio de grua dentro y fuera de la ciudad.",
    address: null,
    phone: "974810871",
    location: {
      lat: 21.030746,
      lon: 105.811913,
    },
    thumb: "assets/img/emergency/thumb/default.jpg",
    cagegories: [
      {
        name:'gruas',
      }
    ],
      open: "Todo el día",
      close:"Toda la noche"
  },
    {
        id: 2,
        name: "GRUA SERGIO SANCHEZ",
        description: "Servicio de grua dentro y fuera de la ciudad.",
        address: null,
        phone: "978421472",
        location: {
            lat: 21.030746,
            lon: 105.811913,
        },
        thumb: "assets/img/emergency/thumb/default.jpg",
        cagegories: [
            {
                name:'gruas',
            }
        ],
        open: "Todo el día",
        close:"Toda la noche"
    },
    {
        id: 3,
        name: "MARCELO CHAVEZ CERRAJERO",
        description: "Servicio de duplicado de llaves, cerrajería, entre otros.",
        address: null,
        phone: "974810871",
        location: {
            lat: 21.030746,
            lon: 105.811913,
        },
        thumb: "assets/img/emergency/thumb/default.jpg",
        cagegories: [
            {
                name:'cerrajero',
            },{
                name:'cerrajeria',
            }

        ],
        open: "Todo el día",
        close:"Toda la noche"
    },

]
