export let HOTELS = [
  {
    id: 1,
    name: "HETTICH APART HOTEL",
    location: {
      lat: -39.825736,
      lon: -73.222048,
    },
    address: "Hettich #325, Valdivia",
      phone:"632214734",
    description: "Departamentos con baño privado, TV cable, servicio de mucama, Wifi, calefacción central y amplio estacionamiento. Habitaciones totalmente equipadas",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"apart hotel"
          },
          {
            name:"alojamiento"
          }
      ]
  },
  {
    id: 2,
    name: "APART HOTEL CASABLANCA",
    location: {
      lat: -39.814008,
      lon: -73.239744,
    },
    address: "GarcÍa Reyes #406, Valdivia",
      phone:"632215393",
    description: "Esta pintoresca casa de verano ofrece alojamiento acogedor a 300 mts de la Plaza de Valdivia, Wifi y estacionamiento privado",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"apart hotel"
          },
          {
            name:"alojamiento"
          }
      ]
  },
  {
    id: 3,
    name: "CABAÑAS 608",
    location: {
      lat: -39.822612,
      lon: -39.822612,
    },
    address: "Baquedano #608, Valdivia",
      phone:"632251975",
    description: "Un establecimiento formal que cuenta con cabañas independientes insertas en un grato ambiente a sólo 12 minutos del centro, caminando cerca del hospital regional",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"cabañas"
          },
          {
            name:"alojamiento"
          }
      ]
  },
  {
    id: 4,
    name: "CAMPING EL AMIGO",
    location: {
      lat: -39.81304,
      lon: -73.226488,
    },
    address: "Bombero Hernández 125, Valdivia",
      phone:"632219994",
    description: "Camping seguro, cerca del puente, es limpio aunque con ducha caliente solo de 8 a 11. Se puede llegar hasta las 00.00 o sino quedas fuera. Buen precio (5000) y tienen pan amasado en una casa al frente.",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"camping"
          },
          {
            name:"alojamiento"
          }
      ]
  },
  {
    id: 5,
    name: "HOSPEDAJE CENTRO",
    location: {
      lat: -39.816668,
      lon: -73.238584,
    },
    address: "Beauchef #686, Valdivia",
      phone:"632209859",
    description: "Hospedaje a una cuadra del Mall Plaza de Los Ríos.",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"hospedaje"
          },
          {
            name:"alojamiento"
          }
      ]
  },
  {
    id: 6,
    name: "HOSTAL COSTAMIA",
    location: {
      lat: -39.815792,
      lon: -73.23484,
    },
    address: "José Marti #57, Valdivia",
      phone:"632430291",
    description: "A solo 3 minutos del Terminal de Buses y a media cuadra de la Costanera de Valdivia, un lugar donde podrás sentirte como en casa.",
    location_text: "",
    thumb: "assets/img/hotel/thumb/img_1.jpg",
    images: [
      "assets/img/hotel/thumb/img_2.jpg",
      "assets/img/hotel/thumb/img_4.jpg",
      "assets/img/hotel/thumb/img_5.jpg",
      "assets/img/hotel/thumb/img_6.jpg"
    ],
    services: [],
      categories:[
          {
            name:"hostal"
          },
          {
            name:"alojamiento"
          }
      ]
  },
];
